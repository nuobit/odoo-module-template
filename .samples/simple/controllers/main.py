# Copyright 2023 NuoBiT Solutions S.L.
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl-3.0)


from odoo import http


class MyController(http.Controller):
    pass
