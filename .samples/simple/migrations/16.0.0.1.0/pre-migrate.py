# Copyright 2023 NuoBiT Solutions S.L.
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl-3.0)


def migrate(cr, version):
    """Update database from previous versions, before updating module."""
    pass
