# Odoo Module Template

This template is made to let you produce an Odoo module scaffolding with all bells and
whistles.

Use it with:

    copier copy https://gitlab.com/nuobit/odoo-module-template ./new_module

Answer the questions. Delete what you don't need. Modify the samples. Commit your new
module! 🚀

The template should be mostly ready to work with latest
[OCA](https://odoo-community.org/) recommendations.
